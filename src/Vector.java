import java.util.Random;

public class Vector {
    public static int generateSizeOfVector() {
        Random generator = new Random();
        int max = 10;
        int min = 5;
        int num = min + generator.nextInt((max - min) + 1);
        return num;
    }

    public static void insertDataToVector(double[] vector) {
        int i = 0;
        Random generator = new Random();
        while(true) {
            try {
                vector[i] = generator.nextDouble();
                i++;
            } catch (ArrayIndexOutOfBoundsException e) {
                System.out.println("Zlapano wyjatek ArrayIndexOutOfBoundsException");
                System.out.println("Koniec wprowadzania danych");
                return;
            }
        }
    }

    public static void showVector(double[] vector) {
        Integer i = 0;
        while(true) {
            try {
                System.out.println("TAB[" + i.toString() + "]: " + String.valueOf(vector[i]));
                i++;
            } catch (ArrayIndexOutOfBoundsException e) {
                System.out.println("Zlapano wyjatek ArrayIndexOutOfBoundsException");
                return;
            }
        }
    }

    public static void main(String[] args) {
        System.out.println();
        System.out.println("Generuje rozmiar wektora...");
        double[] vector = new double[generateSizeOfVector()];

        System.out.println("Wprowadzam dane do wektora...");
        insertDataToVector(vector);

        System.out.println("Wyswietlam zawartosc wektora");
        showVector(vector);
    }
}
