interface Quiz {
    int MIN_VALUE = 0;    // minimalny zakres poszukiwan
    int MAX_VALUE = 1000; // maksymalny zakres poszukiwan

    void isCorrectValue(int value) throws Quiz.ParamTooLarge, Quiz.ParamTooSmall;

    class ParamTooLarge extends Exception {}
    class ParamTooSmall extends Exception {}
}
