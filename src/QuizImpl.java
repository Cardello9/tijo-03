public class QuizImpl implements Quiz {

    private int digit;

    public QuizImpl() {
        this.digit = 254;
    }

    public void isCorrectValue(int value) throws Quiz.ParamTooLarge, Quiz.ParamTooSmall {
        if(value > this.digit) {
            throw new Quiz.ParamTooLarge();
        } else if (value < this.digit) {
            throw new Quiz.ParamTooSmall();
        } else {
            return;
        }
    }
}
